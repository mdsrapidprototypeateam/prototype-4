﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour {

	public DefenseUnit unitToUpgrade;
	public Button upgradeA, upgradeB, finalUpgrade;
	public GameObject textObjectA, textObjectB;
	public TMPro.TextMeshProUGUI upgradeTextA1, upgradeTextB1, upgradeTextA2, upgradeTextB2;
    TMPro.TextMeshProUGUI[] upgradeTexts = new TMPro.TextMeshProUGUI[4];
    Image[] sprites = new Image[4];
    public List<Sprite> resourceImages;
    PlaceObject player;


	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlaceObject>();
        upgradeTexts[0] = upgradeTextA1;
        upgradeTexts[1] = upgradeTextB1;
        upgradeTexts[2] = upgradeTextA2;
        upgradeTexts[3] = upgradeTextB2;
        sprites[0] = textObjectA.transform.GetChild(2).GetComponent<Image>();
        sprites[1] = textObjectA.transform.GetChild(3).GetComponent<Image>();
        sprites[2] = textObjectB.transform.GetChild(2).GetComponent<Image>();
        sprites[3] = textObjectB.transform.GetChild(3).GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
        TextUpdate(unitToUpgrade);
	}

	public void DoUpgrade(UpgradeBranch branch){
        
        UpgradeBranch trueBranch = branch;
        if(unitToUpgrade.upgradeTier > 0)
        {
            trueBranch = unitToUpgrade.upgradeBranch;
        }
		unitToUpgrade.DoUpgrade(unitToUpgrade.upgradeTier+1, trueBranch);
        foreach (UpgradeResource r in unitToUpgrade.branchUpgrades[(int)trueBranch - 1].tiers[unitToUpgrade.upgradeTier].upgradeResources)
        {
            RemoveResource(r);
        }
        unitToUpgrade.upgradeTier++;
        unitToUpgrade.upgradeBranch = trueBranch;
        DefenseUnit unit = unitToUpgrade;
        DeselectUnit();
        GetUnit(unit);
	}

    public void GetUnit(DefenseUnit unit)
    {
        unitToUpgrade = unit;
        switch (unit.upgradeTier)
        {
            case 0:
                upgradeA.gameObject.SetActive(true);
                upgradeB.gameObject.SetActive(true);
                textObjectA.SetActive(true);
                textObjectB.SetActive(true);
                upgradeTextA1.text = unitToUpgrade.branchUpgrades[0].tiers[0].upgradeResources[0].resourceCost.ToString();
                upgradeTextA2.text = unitToUpgrade.branchUpgrades[1].tiers[0].upgradeResources[0].resourceCost.ToString();
                upgradeTextB1.text = unitToUpgrade.branchUpgrades[0].tiers[0].upgradeResources[1].resourceCost.ToString();
                upgradeTextB2.text = unitToUpgrade.branchUpgrades[1].tiers[0].upgradeResources[1].resourceCost.ToString();
                break;
            case 1:
                finalUpgrade.gameObject.SetActive(true);
                textObjectA.SetActive(true);
                upgradeTextA1.text = unitToUpgrade.branchUpgrades[(int)unitToUpgrade.upgradeBranch-1].tiers[1].upgradeResources[0].resourceCost.ToString();
                upgradeTextB1.text = unitToUpgrade.branchUpgrades[(int)unitToUpgrade.upgradeBranch-1].tiers[1].upgradeResources[1].resourceCost.ToString();
                break;
        }

        
    }

    public void DeselectUnit()
    {
        
        upgradeA.gameObject.SetActive(false);
        upgradeB.gameObject.SetActive(false);
        textObjectA.SetActive(false);
        textObjectB.SetActive(false);
        finalUpgrade.gameObject.SetActive(false);
        unitToUpgrade = null;
    }

    bool CheckResource(UpgradeResource r)
    {
        switch (r.resourceType)
        {
            case ResourceType.Gold:
                return (player.gold < r.resourceCost);
            case ResourceType.Oil:
                return (player.oil < r.resourceCost);
            case ResourceType.Ore:
                return (player.ore < r.resourceCost);
        }

        return false;
    }

    Sprite SetSprite(UpgradeResource r)
    {
        switch (r.resourceType)
        {
            case ResourceType.Gold:
                return resourceImages[0];
            case ResourceType.Oil:
                return resourceImages[1];
            case ResourceType.Ore:
                return resourceImages[2];
        }

        return null;
    }

    void RemoveResource(UpgradeResource r)
    {
        switch (r.resourceType)
        {
            case ResourceType.Gold:
                player.gold -= r.resourceCost;
                break;
            case ResourceType.Oil:
                player.oil -= r.resourceCost;
                break;
            case ResourceType.Ore:
                player.ore -= r.resourceCost;
                break; 
        }
    }


    void TextUpdate(DefenseUnit unitToUpgrade)
    {
        if (unitToUpgrade)
        {
            bool canUpgrade = false;
            switch (unitToUpgrade.upgradeTier)
            {
                case 0:
                    for (int i = 0; i < unitToUpgrade.branchUpgrades.Count; i++)
                    {
                        canUpgrade = true;
                        for (int j = 0; j < unitToUpgrade.branchUpgrades[i].tiers[0].upgradeResources.Count; j++)
                        {
                            if (CheckResource(unitToUpgrade.branchUpgrades[i].tiers[0].upgradeResources[j]))
                            {
                                upgradeTexts[(2 * i + j)].color = Color.red;
                                canUpgrade = false;
                            }
                            else
                            {
                                upgradeTexts[(2 * i + j)].color = Color.white;
                            }

                            sprites[(2 * i + j)].sprite = SetSprite(unitToUpgrade.branchUpgrades[i].tiers[0].upgradeResources[j]);

                        }
                        if (i == 0)
                        {
                            upgradeA.interactable = canUpgrade;
                        } else
                        {
                            upgradeB.interactable = canUpgrade;
                        }
                    }
                    break;
                case 1:
                    canUpgrade = true;
                    for (int j = 0; j < unitToUpgrade.branchUpgrades[(int)unitToUpgrade.upgradeBranch - 1].tiers[1].upgradeResources.Count; j++)
                    {
                        if (CheckResource(unitToUpgrade.branchUpgrades[(int)unitToUpgrade.upgradeBranch - 1].tiers[1].upgradeResources[j]))
                        {
                            upgradeTexts[(j)].color = Color.red;
                            canUpgrade = false;
                        }
                        else
                        {
                            upgradeTexts[(j)].color = Color.white;
                        }

                        sprites[(j)].sprite = SetSprite(unitToUpgrade.branchUpgrades[(int)unitToUpgrade.upgradeBranch - 1].tiers[1].upgradeResources[j]);
                    }
                    finalUpgrade.interactable = canUpgrade;
                    break;
            }
        }
    }
}
