﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeButtonBranch : MonoBehaviour {
    public UpgradeBranch branch;
    public UpgradeManager upgradeManager;

    public void DoUpgrade()
    {
        upgradeManager.DoUpgrade(branch);
    }
}
