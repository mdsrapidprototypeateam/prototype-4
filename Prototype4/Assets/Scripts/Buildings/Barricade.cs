﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barricade : DefenseUnit {

    public LayerMask mask;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(health <= 0)
        {
            Death();
        }
	}

    void EnemiesNear()
    {
        Collider[] colls = Physics.OverlapSphere(transform.position, attackRadius, mask);
        foreach (Collider c in colls)
        {
            if (c.GetComponent<UnitStats>())
            {
                if (!c.GetComponent<UnitBase>().attackRange)
                {
                    c.GetComponent<UnitBase>().attackRange = true;
                    c.GetComponent<UnitBase>().attackTarget = gameObject;
                }
            }
        }
    }

    void Death()
    {
        Collider[] colls = Physics.OverlapSphere(transform.position, attackRadius, mask);
        foreach (Collider c in colls)
        {
            if (c.GetComponent<UnitStats>())
            {
                if (c.GetComponent<UnitBase>().attackRange)
                {
                    c.GetComponent<UnitBase>().attackRange = false;
                    c.GetComponent<UnitBase>().attackTarget = null;
                }
            }
        }

        Destroy(gameObject);
    }

    private void FixedUpdate()
    {
        EnemiesNear();
    }
}
