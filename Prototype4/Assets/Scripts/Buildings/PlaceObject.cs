﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceObject : MonoBehaviour {

    public DefenseUnit unit;
    public DefenseUnit selectedUnit;

    public GameObject preview;
    public Material mouseOverMat;
    public Material normalTileMat;
    public Transform currentTile_MouseOver;
	public SpriteRenderer selectedSprite;

    public int gold;
	public int oil;
	public int ore;

	public UpgradeManager upMan;

    public ConstructionPanel constructionPanel;

    public float raycastDistance = 20000f;

	public LayerMask mask;
	public LayerMask ignoreBuildingMask;

    // Use this for initialization
    void Start () {
        upMan = FindObjectOfType<UpgradeManager>();
	}
	
	// Update is called once per frame
	void Update () {

		
		
		if (Input.GetButtonDown ("Confirm")) {
				
			RaycastHit hit;

			if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject ()) {

				if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, raycastDistance, mask)) {
                        
                    if (hit.transform.tag == "tile")
                    {
                        TileSettings tile = hit.transform.GetComponent<TileSettings>();
                        if (!hit.transform.GetComponent<TileSettings>().isTaken)
                        {

                            constructionPanel.gameObject.SetActive(true);
                            
                            constructionPanel.tile = tile;
                            constructionPanel.type = tile.type;
                            constructionPanel.EnableButtons();
                        }
                    }
                        
                    if (hit.transform.GetComponent<DefenseUnit> () != null) {
						if (selectedUnit) {
							selectedUnit.tile.SetActive (true);
						}
						selectedUnit = hit.transform.GetComponent<DefenseUnit> ();
						selectedSprite.transform.position = selectedUnit.tile.transform.position;
						selectedSprite.gameObject.SetActive (true);
                        Color currentColor = selectedUnit.tile.GetComponent<Renderer>().material.color;
                        selectedSprite.GetComponent<Renderer>().material.color = new Color(currentColor.r, currentColor.g, currentColor.b, 1);
                        selectedUnit.tile.SetActive (false);
                        upMan.GetUnit(selectedUnit);
					}
				}
			}
		}

		if (Input.GetButtonDown ("Cancel")) {
            constructionPanel.gameObject.SetActive(false);

            if (selectedUnit)
            {
                selectedUnit.tile.SetActive(true);
                selectedSprite.gameObject.SetActive(false);
                selectedUnit = null;
                upMan.DeselectUnit();
            }
		}
		
	}

    
    private void FixedUpdate()
    {
        RaycastHit hit;
        
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, raycastDistance, mask))
        {
            if (hit.transform.tag == "tile")
            {
                TileSettings tile = hit.transform.GetComponent<TileSettings>();
                Color currentColor;
                if (currentTile_MouseOver)
                {
                    currentColor = currentTile_MouseOver.GetComponent<Renderer>().material.color;
                    currentTile_MouseOver.GetComponent<Renderer>().material.color = new Color(currentColor.r, currentColor.g, currentColor.b, 0.10f);
                    currentTile_MouseOver.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
                }
                currentTile_MouseOver = tile.transform;
                currentColor = currentTile_MouseOver.GetComponent<Renderer>().material.color;
                currentTile_MouseOver.GetComponent<Renderer>().material.color = new Color(currentColor.r, currentColor.g, currentColor.b, 1f);
                currentTile_MouseOver.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white * 0.2f);
            }

            if (hit.transform.GetComponent<DefenseUnit>() != null)
            {
				if (hit.transform.GetComponent<Palace> () == null && hit.transform.GetComponent<WatchTower>() == null) 
				{
					Color currentColor;
					if (currentTile_MouseOver)
					{
						currentColor = currentTile_MouseOver.GetComponent<Renderer>().material.color;
						currentTile_MouseOver.GetComponent<Renderer>().material.color = new Color(currentColor.r, currentColor.g, currentColor.b, 0.10f);
					}
					currentTile_MouseOver = hit.transform.GetComponent<DefenseUnit>().tile.transform;
					currentColor = currentTile_MouseOver.GetComponent<Renderer>().material.color;
					currentTile_MouseOver.GetComponent<Renderer>().material.color = new Color(currentColor.r, currentColor.g, currentColor.b, 1f);
				}
            
            }


        }

       
    }

    public void SpawnBuilding(DefenseUnit u, TileSettings t)
    {
        
        
		DefenseUnit newUnit = Instantiate(u, t.transform.position, Quaternion.Euler(new Vector3(0,-90,0)));
        t.isTaken = true;
        GetComponent<AudioEvents>().ConstructionSound();
        newUnit.placer = this;
        newUnit.tile = t.gameObject;
        gold -= newUnit.cost;
        
    }
}
