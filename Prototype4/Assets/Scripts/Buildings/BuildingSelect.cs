﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingSelect : MonoBehaviour {

    public DefenseUnit unit;
    public GameObject unitPreview;
    public PlaceObject placingScript;

    Button button;

    public int goldCost;

	// Use this for initialization
	void Start () {
		button = GetComponent<Button>();
        goldCost = unit.cost;
        unitPreview.GetComponent<Preview>().radius = unit.attackRadius;
    }
	
	// Update is called once per frame
	void Update () {
		if (unit.cost > placingScript.gold)
        {
            button.interactable = false;
        } else
        {
            button.interactable = true;
        }
	}

    public void OnButtonClick()
    {
        placingScript.unit = unit;
        if (placingScript.preview)
        {
            Destroy(placingScript.preview.gameObject);
        }
        placingScript.preview = Instantiate(unitPreview);
    }
}
