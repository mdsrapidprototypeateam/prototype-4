﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Upgrade Tier", menuName = "Upgrade Tier", order = 1)]
public class UpgradeTier : ScriptableObject {
    public List<UpgradeTemplate> upgrades;
    public List<UpgradeResource> upgradeResources;
}

[System.Serializable]
public class TierList
{
    public List<UpgradeTier> tiers;
}
