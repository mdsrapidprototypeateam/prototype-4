﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StandardShaderUtils
{
    public enum BlendMode
    {
        Opaque,
        Cutout,
        Fade,
        Transparent
    }

    public static void ChangeRenderMode(Material standardShaderMaterial, BlendMode blendMode)
    {
        switch (blendMode)
        {
            case BlendMode.Opaque:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                standardShaderMaterial.SetInt("_ZWrite", 1);
                standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = -1;
                break;
            case BlendMode.Cutout:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                standardShaderMaterial.SetInt("_ZWrite", 1);
                standardShaderMaterial.EnableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = 2450;
                break;
            case BlendMode.Fade:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                standardShaderMaterial.SetInt("_ZWrite", 0);
                standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.EnableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = 3000;
                break;
            case BlendMode.Transparent:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                standardShaderMaterial.SetInt("_ZWrite", 0);
                standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = 3000;
                break;
        }

    }
}

public class Preview : MonoBehaviour {

    public float blinkRate;
    public List<Material> mat;
    public float radius;
    public GameObject range;

	// Use this for initialization
	void Start () {
        mat = new List<Material>();
        //Material[] tempMat = GetComponent<Renderer>().materials;
        //mat.AddRange(tempMat);
        Renderer[] childrenRend = GetComponentsInChildren<Renderer>();
        for(int i = 0; i < childrenRend.Length; i++)
        {
            mat.AddRange(childrenRend[i].materials);
        }

        foreach(Material m in mat)
        {
            StandardShaderUtils.ChangeRenderMode(m, StandardShaderUtils.BlendMode.Transparent);
        }

        if (range)
        {
            range.transform.localScale = new Vector3(radius * 2 * (1 / transform.localScale.x), 0.01f * (1 / transform.localScale.y), radius * 2 * (1 / transform.localScale.z));
        }
	}
	
	// Update is called once per frame
	void Update () {
        float alpha = Mathf.Abs(Mathf.Sin(Time.unscaledTime * blinkRate) * 0.6f) + 0.2f;

        foreach (Material m in mat)
        {
            m.color = new Color(m.color.r, m.color.g, m.color.b, alpha);
        }
    }
}
