﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceType
{
    Ore,
    Oil,
    Gold
}

public enum UpgradeBranch
{
    None = 0,
    Left = 1,
    Right = 2,
}

[System.Serializable]
public struct UpgradeResource
{
    public int resourceCost;
    public ResourceType resourceType;
}

[CreateAssetMenu(fileName = "Upgrade", menuName = "Upgrade Object", order = 1)]
public class UpgradeTemplate : ScriptableObject
{
    public string upgradeName;
    public float upgradeValue;

};
