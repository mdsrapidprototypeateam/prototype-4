﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Farm : DefenseUnit {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (canShoot)
        {
            StartCoroutine(MoneyCooldown());
        }
	}

    IEnumerator MoneyCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(attackRate);
        placer.gold += (int)baseDamage;
        canShoot = true;

    }
}
