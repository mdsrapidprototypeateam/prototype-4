﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Palace : DefenseUnit {

    public LineRenderer targetLine;
    public CannonBall ballPrefab;
    public Transform ballSpawnPosition;

    public LayerMask mask;

    UnitStats target;

    // Use this for initialization
    void Start () {
        enemies = new List<UnitStats>();
        radiusIndicator.transform.localScale = new Vector3(attackRadius * 2 * (1 / transform.localScale.x), 0.01f * (1 / transform.localScale.y), attackRadius * 2 * (1 / transform.localScale.z));
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Death();
        }

        List<UnitStats> toRemove = new List<UnitStats>();
        foreach (UnitStats enemy in enemies)
        {
            if (enemy.health <= 0)
            {
                toRemove.Add(enemy);
            }
        }

        foreach (UnitStats enemy in toRemove)
        {
            enemies.Remove(enemy);
        }

        if (enemies.Count > 0)
        {
            if (target)
            {
                targetLine.enabled = true;
                targetLine.SetPosition(0, transform.position);
                targetLine.SetPosition(1, target.transform.position);
            }
            if (canShoot)
            {
                Fire();
                AudioManager.Instance.TowerShot(transform.position);
            }
        }
        else
        {
            targetLine.enabled = false;
        }
    }

    void EnemiesNear()
    {
        Collider[] colls = Physics.OverlapSphere(transform.position, attackRadius, mask);
        enemies.Clear();
        foreach (Collider c in colls)
        {  
            if (c.GetComponent<UnitStats>())
            {
                enemies.Add(c.GetComponent<UnitStats>());
                if (!c.GetComponent<UnitBase>().attackRange)
                {
                    c.GetComponent<UnitBase>().attackRange = true;
                    c.GetComponent<UnitBase>().attackTarget = gameObject;
                }
            }
        }
    }

    void Death()
    {
        Collider[] colls = Physics.OverlapSphere(transform.position, attackRadius, mask);
        foreach (Collider c in colls)
        {
            if (c.GetComponent<UnitStats>())
            {
                if (c.GetComponent<UnitBase>().attackRange)
                {
                    c.GetComponent<UnitBase>().attackRange = false;
                    c.GetComponent<UnitBase>().attackTarget = null;
                }
            }
        }

        //Destroy(gameObject);
    }

    private void FixedUpdate()
    {
        EnemiesNear();
        target = CalculateTarget();
    }

    IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(attackRate);
        if (target)
        {
            target.health -= baseDamage;
        }
        canShoot = true;

    }

    public override void Fire()
    {
        base.Fire();

        CannonBall ball = Instantiate(ballPrefab, ballSpawnPosition.position, Quaternion.identity);
        ball.target = target.transform;
        StartCoroutine(ShootCooldown());
    }

    private UnitStats CalculateTarget()
    {
        UnitStats closest = null;

        if (enemies.Count != 0)
        {
            closest = enemies[enemies.Count - 1];
        }

        return closest;
    }
}
