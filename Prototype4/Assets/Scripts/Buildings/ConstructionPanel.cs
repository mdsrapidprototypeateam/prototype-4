﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConstructionPanel : MonoBehaviour {

	public GameObject[] buttons = new GameObject[6];
    public DefenseUnit[] buildings = new DefenseUnit[6];
    public BuildingType type;
    public TileSettings tile;
    PlaceObject player;
    public int cost;
    public TMPro.TextMeshProUGUI costText;
	public TMPro.TextMeshProUGUI descText;

    private void Start()
    {
        player = FindObjectOfType<PlaceObject>();
    }

    private void Update()
    {
        if (costText.IsActive())
        {
            if (player.gold < cost)
            {
                costText.color = Color.red;
            } else
            {
                costText.color = Color.yellow;
            }
        }
    }

    public void EnableButtons()
    {
        foreach(GameObject g in buttons)
        {
            if(type == g.GetComponent<BuildType>().type)
            {
                g.GetComponent<Button>().interactable = true;
            } else
            {
                g.GetComponent<Button>().interactable = false;
            }
        }
    }

    public void OnClick(int i)
    {
        if (player.gold >= buildings[i].cost)
        {
            player.SpawnBuilding(buildings[i], tile);
            gameObject.SetActive(false);
        }
    }

    public void UpdateCost(int i)
    {
        costText.gameObject.SetActive(true);
        cost = buildings[i].cost;
        costText.text = cost.ToString();
    }

    public void CleanCost()
    {
        costText.gameObject.SetActive(false);
    }

	public void UpdateDescription(int i)
	{
		descText.gameObject.SetActive(true);
		descText.text = buildings [i].description.text;

	}

	public void CleanDescription()
	{
		descText.gameObject.SetActive(false);

	}
}
