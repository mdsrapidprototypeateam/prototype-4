﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreMiner : DefenseUnit {

    private void Update()
    {
        Fire();
    }
    public override void Fire()
    {
        if (canShoot)
        {
            placer.ore += (int)baseDamage;
            StartCoroutine(ShootCooldown());
        }
    }

    IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(attackRate);
        canShoot = true;
    }
}
