﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchTower : DefenseUnit {

    public LineRenderer targetLine;
    public CannonBall ballPrefab;
    public Transform ballSpawnPosition;
    public float explosiveDamage;
    public float explosiveRadius;

    public LayerMask mask;

    public UnitStats target;

	public GameObject effects;

	// Use this for initialization
	void Start () {
        enemies = new List<UnitStats>();
        radiusIndicator.transform.localScale = new Vector3(attackRadius * 2, 0.01f, attackRadius * 2);
        ballSpawnPosition = upgradeModels[0].ballSpawner.transform;
    }
	
	// Update is called once per frame
	void Update () {
        List<UnitStats> toRemove = new List<UnitStats>();
        foreach(UnitStats enemy in enemies)
        {
            if(enemy.health <= 0)
            {
                toRemove.Add(enemy);
            }
        }

        foreach(UnitStats enemy in toRemove)
        {
            enemies.Remove(enemy);
        }

        if (enemies.Count > 0)
        {
            if (target)
            {
                //targetLine.enabled = true;

                targetLine.SetPosition(0, transform.position);
                targetLine.SetPosition(1, target.transform.position);
				effects.SetActive (true);
				effects.transform.position = ballSpawnPosition.position;
				effects.transform.LookAt (target.transform.position);
				if (canShoot)
				{
					Fire();
					AudioManager.Instance.RocketShot(transform.position);
				}
            }
            
        } else
        {
            targetLine.enabled = false;
			effects.SetActive (false);
        }
	}

    private void FixedUpdate()
    {
        FindEnemies();
        target = CalculateTarget();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<UnitStats>())
        {
            enemies.Add(other.GetComponent<UnitStats>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<UnitStats>())
        {
            enemies.Remove(other.GetComponent<UnitStats>());
        }
    }

    IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(attackRate);
        //target.health -= baseDamage;

        if (target)
        {
            Collider[] coll = Physics.OverlapSphere(target.transform.position, explosiveRadius, mask);
            foreach (Collider c in coll)
            {
                UnitStats s;
                if (s = c.GetComponent<UnitStats>())
                {
                    s.health -= baseDamage;
                }
            }
        }
        canShoot = true;
        
    }

    public override void Fire()
    {
        base.Fire();
        
        CannonBall ball = Instantiate(ballPrefab, ballSpawnPosition.position, Quaternion.identity);
        ball.target = target.transform;
        StartCoroutine(ShootCooldown());
    }

    public void FindEnemies()
    {
        Collider[] enemyCol = Physics.OverlapSphere(transform.position, attackRadius, mask);
        enemies.Clear();
        foreach(Collider c in enemyCol)
        {
            if (c.GetComponent<UnitStats>())
            {
                enemies.Add(c.GetComponent<UnitStats>());
            }
            
        }
    }

    public override void DoUpgrade(int tier, UpgradeBranch branch)
    {
        base.DoUpgrade(tier, branch);
        ballSpawnPosition = upgradeModels[activeModel].ballSpawner.transform;
    }

    private UnitStats CalculateTarget()
    {
		UnitStats closest = null;

		if(!enemies.Contains(target)){

			if (enemies.Count != 0)
			{
				closest = enemies[enemies.Count - 1];
			}
		}

		return closest;
    }
}
