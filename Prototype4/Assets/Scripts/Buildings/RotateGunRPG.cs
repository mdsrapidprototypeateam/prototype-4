﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateGunRPG : MonoBehaviour {

	public WatchTower mainBuilding;

	// Use this for initialization
	void Start () {
		mainBuilding = transform.parent.parent.GetComponent<WatchTower> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (mainBuilding.target) {
			Quaternion rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (mainBuilding.target.transform.position - transform.position, transform.up), Time.deltaTime * 10f);
			Vector3 eulerRot = rotation.eulerAngles;
			eulerRot.x = 0;
			eulerRot.z = 0;
			transform.rotation = Quaternion.Euler(eulerRot);
		}
	}
}
