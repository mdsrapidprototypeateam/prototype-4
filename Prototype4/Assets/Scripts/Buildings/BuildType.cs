﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildType : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public BuildingType type;
    public int buildingIndex;
    public ConstructionPanel constructionPanel;


    private void Start()
    {
        constructionPanel = FindObjectOfType<ConstructionPanel>();
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        constructionPanel.UpdateCost(buildingIndex);
		constructionPanel.UpdateDescription(buildingIndex);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        constructionPanel.CleanCost();
		constructionPanel.CleanDescription ();
    }
}
