﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OilMiner : DefenseUnit {

    private void Update()
    {
        Fire();
    }
    public override void Fire()
    {
        if (canShoot)
        {
            placer.oil += (int)baseDamage;
            StartCoroutine(ShootCooldown());
        }
    }

    IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(attackRate);
        canShoot = true;
    }
}
