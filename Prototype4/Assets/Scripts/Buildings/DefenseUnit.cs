﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BuildingType
{
    Tower,
    Trap,
    Gather
}

public class DefenseUnit : MonoBehaviour
{
    [Header("Defense Stats")]
    public float attackRadius;
    public float attackRate;
    public float baseDamage;
    public float health;
    public int cost;

    public BuildingType type;

    public PlaceObject placer;

    public GameObject radiusIndicator;

    public List<UnitStats> enemies;
    public bool canShoot = true;

	public GameObject tile;

	public int upgradeTier;
	public UpgradeBranch upgradeBranch = UpgradeBranch.None;

    public List<TierList> branchUpgrades;
    public List<PartUpgrade> upgradeModels;
    protected int activeModel;

	public Text description;
	
    public virtual void Fire()
    {

    }
		
	public virtual void DoUpgrade(int tier, UpgradeBranch branch)
	{
		
		if (branchUpgrades[(int)branch - 1].tiers.Count > 0) {
			foreach (UpgradeTemplate u in branchUpgrades[(int)branch - 1].tiers[tier-1].upgrades) {
				if (u.upgradeName == "Damage Up") {
					baseDamage = u.upgradeValue;
				}

                else if (u.upgradeName == "Rate Up")
                {
                    attackRate = u.upgradeValue;
                }
			}
		}

        activeModel++;
        upgradeModels[activeModel - 1].gameObject.SetActive(false);
        upgradeModels[activeModel].gameObject.SetActive(true);

    }
}
