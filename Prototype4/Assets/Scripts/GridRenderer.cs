﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridRenderer : MonoBehaviour {

    public Transform TileParent;
    public PathNode pathNode;
    public Material texture;
    public float tileXDim = 10.0f;
    public float tileZDim = 10.0f;
	public float tileScale = 2.0f;
    public int rowCount = 20;
    public int colCount = 20;

    public Vector3 startPosition;
    private Vector3 startPositionReset;

    // Use this for initialization
    void Start () {

        startPosition = pathNode.transform.position;
        startPositionReset = startPosition;
        rowCount *= 2;
        //startPosition += new Vector3(- Mathf.Ceil(colCount / 2.0f) * tileXDim, 0.0f, - Mathf.Ceil(rowCount / 2.0f) * tileZDim);
        bool isRowEven = false;
        for (int j = 0; j < rowCount; j++)
        {
            isRowEven = !isRowEven;
            startPosition = startPosition + new Vector3((!isRowEven ? tileXDim * -0.5f : 0), 0, (!isRowEven ? tileZDim : 0));
            for (int k = 0; k < colCount; k += 2)
            {
				pathNode.cell = GameObject.CreatePrimitive(PrimitiveType.Plane); // Hex GameOjbect
                pathNode.cell.tag = "tile";
                pathNode.cell.layer = LayerMask.NameToLayer("Grid");
                pathNode.cell.AddComponent<TileSettings>();
                pathNode.cell.transform.parent = TileParent;
                pathNode.cell.transform.position = startPosition + new Vector3(tileXDim * j + (tileXDim*0.5f), 0,tileZDim * k);
				pathNode.cell.transform.localScale = new Vector3 (tileScale,1.0f,tileScale);
				/*Vector3 pos0= new Vector3();

				RaycastHit hit0;
				if (Physics.Raycast(pos0, -Vector3.up, out hit0, 1000.0F))
				{
					pathNode.height = Terrain.activeTerrain.SampleHeight(pos0);
				}*/



                pathNode.cell.GetComponent<Renderer>().material = texture;
				pathNode.cell.GetComponent<MeshCollider> ().convex = true;
				pathNode.cell.GetComponent<MeshCollider> ().isTrigger = true;

                //THIS LAYS OUT THE CELLS OVER THE TERRAIN
                Mesh mesh = ((MeshFilter)pathNode.cell.GetComponent(typeof(MeshFilter))).mesh as Mesh;
                Vector3[] vertices = mesh.vertices;
                Vector3 position = new Vector3(pathNode.cell.transform.position.x + (pathNode.cell.transform.localScale.x * 10 / 2), 1000, pathNode.cell.transform.position.z + (pathNode.cell.transform.localScale.z * 10 / 2));
                float xStep = pathNode.cell.transform.localScale.x;
                float zStep = pathNode.cell.transform.localScale.z;
                int squaresize = 10 + 1;
                RaycastHit hit;
                for (int n = 0; n < squaresize; n++)
                {
                    for (int i = 0; i < squaresize; i++)
                    {
                        if (Physics.Raycast(position, -Vector3.up, out hit, 1000.0F))
                        {
                            vertices[(n * squaresize) + i].y = Terrain.activeTerrain.SampleHeight(position);
                            position.x -= xStep;
                        }
                    }
                    position.x += (((float)squaresize) * xStep);
                    position.z -= zStep;
                }

                mesh.vertices = vertices;
                mesh.RecalculateBounds();
				pathNode.cell.transform.GetComponent<TileSettings> ().bounds = pathNode.cell.GetComponent<Renderer>().bounds;
                pathNode.cell.transform.position += new Vector3(0, 0.3f, 0);
            }
            startPosition = startPosition + new Vector3((!isRowEven ? tileXDim * -0.5f : 0), 0, (!isRowEven ? -tileZDim : 0));
        }
    }
	
	// Update is called once per frame
	void Update () {
        
        
    }
}
