﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSettings : MonoBehaviour {

    public bool isTaken;
	public Bounds bounds;
	public Bounds colliderBounds;

	public List<GameObject> onTop;
	public List<TreeInstance> trees;

    public BuildingType type;

}
