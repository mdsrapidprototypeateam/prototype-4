﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floatingtext : MonoBehaviour 
{
	public float lifeTimer;
	public int type;
	public float movSpeed;
	// Use this for initialization
	void Start () 
	{
		switch (type) 
		{
		//money
		case 0:
			GetComponent<TMPro.TextMeshPro> ().color = Color.yellow;
			break;
		//damage
		case 1:
			GetComponent<TMPro.TextMeshPro> ().color = Color.red;
			break;
		//resources
		case 2:
			GetComponent<TMPro.TextMeshPro> ().color = Color.blue;
			break;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (lifeTimer > 0) 
		{
			Vector3 pos = GetComponent<RectTransform>().position;
			pos.y += movSpeed * Time.deltaTime;

			//GetComponent<RectTransform> ().position = pos;
			lifeTimer -= Time.deltaTime;
		} 
		else 
		{
			Destroy (this.gameObject);
		}
	}
}
