﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_RESOURCES : MonoBehaviour {

    public MyGameManager myGameManager;
	public TextMeshProUGUI textComp;
	public PlaceObject playerComp;
	public UnitSpawner unitSpawnerComp;

	public bool isGold = false;
	public bool isOre = false;
	public bool isOil = false;
	public bool isWave = false;

    // Use this for initialization
    void Start()
    {
        myGameManager = MyGameManager.GetInstance();
		textComp = GetComponent<TextMeshProUGUI> ();
		playerComp = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<PlaceObject> ();
		unitSpawnerComp = GameObject.FindGameObjectWithTag ("spawnerManager").GetComponent<UnitSpawner> ();
    }

    // Update is called once per frame
    void Update () {
		if (isGold) {
			textComp.text = playerComp.gold.ToString ();
		} else if (isOre) {
			textComp.text = playerComp.ore.ToString ();
		} else if (isOil) {
			textComp.text = playerComp.oil.ToString ();
		} else if (isWave && unitSpawnerComp != null) {
			textComp.text = unitSpawnerComp.currentWave.ToString ();
		}
	}
}
