﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadText : MonoBehaviour {

    public Text textComp;
    public Vector3 startPosition;
    public float timeInterval = 50f;
    public float distance = 967;
    public float speed;
    public float maxY;
    public float currY;

	// Use this for initialization
	void Start () {
        textComp = GetComponent<Text>();
        startPosition = transform.position;
        speed = distance / timeInterval;
        maxY = startPosition.y + distance;
    }
	
	// Update is called once per frame
	void Update () {
        currY = textComp.rectTransform.anchoredPosition.y;
        if (currY < maxY)
        {
            Vector3 pos = textComp.rectTransform.anchoredPosition;
            pos += new Vector3(0, speed * Time.deltaTime, 0);
            textComp.rectTransform.anchoredPosition = pos;
        }
        else
            MyGameManager.GetInstance().ChangeToGameStartedState();
    }
}
