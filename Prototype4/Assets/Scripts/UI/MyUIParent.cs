﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyUIParent : MonoBehaviour
{    

	void Start () {
        Canvas canvas = GetComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = GameObject.FindGameObjectWithTag("UI_CAMERA").GetComponent<Camera>();
	}
	
	void Update () {
		
	}
}
