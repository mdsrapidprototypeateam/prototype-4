﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Gold : MonoBehaviour {

    public Text textComponent;
    public PlaceObject placeObjectComp;
    public int goldCount;

	// Use this for initialization
	void Start () {
        textComponent = GetComponent<Text>();
        placeObjectComp = Camera.main.GetComponent<PlaceObject>();
	}
	
	// Update is called once per frame
	void Update () {
        goldCount = placeObjectComp.gold;
        textComponent.text = goldCount.ToString();

    }
}
