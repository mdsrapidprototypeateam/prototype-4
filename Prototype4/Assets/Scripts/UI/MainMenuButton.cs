﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuButton : MonoBehaviour {

    public MyGameManager myGameManager;

    // Use this for initialization
    void Start () {
        myGameManager = MyGameManager.GetInstance();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        myGameManager.ChangeToGameStartedState();
    }
}
