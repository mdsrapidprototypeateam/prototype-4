﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameFinish : MonoBehaviour {

    public Text textComponent;
    public string VictoryString = "You are victorious!!!";
    public string GameoverString = "You lost...";

    public MyGameManager gameManager;

	// Use this for initialization
	void Start () {
        textComponent = GetComponent<Text>();
        gameManager = MyGameManager.GetInstance();
        textComponent.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(gameManager.e_State == EGameState.STATE_FINISH)
        {
            if (gameManager.e_FinishState == EFinishState.WINNER && textComponent.text != VictoryString)
            {
                textComponent.text = VictoryString;
            }
            else if (gameManager.e_FinishState == EFinishState.GAMEOVER && textComponent.text != GameoverString)
            {
                textComponent.text = GameoverString;
            }
            textComponent.enabled = true;
        }
    }
}
