﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_UnitCavasRotator : MonoBehaviour {

    public Vector3 startingRotation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Quaternion.Euler(startingRotation);
	}
}
