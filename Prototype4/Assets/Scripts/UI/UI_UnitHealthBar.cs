﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_UnitHealthBar : MonoBehaviour {

    public Image healthBar;
    public DefenseUnit parentUnit;
    public UnitStats parentAttackerUnit;
    public float healthInit = 0;
    public float healthPerc = 0;

	void Start () {
        healthBar = transform.Find("Bar").GetComponent<Image>();
        parentUnit = transform.parent.GetComponent<DefenseUnit>();
        if (parentUnit)
        {
            healthInit = parentUnit.health;
            healthPerc = 1.0f;
            return;
        }
        parentAttackerUnit = transform.parent.GetComponent<UnitStats>();
        if (parentAttackerUnit)
        {
            healthInit = parentAttackerUnit.health;
            healthPerc = 1.0f;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (parentUnit)
        {
            healthPerc = parentUnit.health / healthInit;
        }
        else
        {
            healthPerc = parentAttackerUnit.health / healthInit;
        }
        healthBar.fillAmount = healthPerc;
    }
}
