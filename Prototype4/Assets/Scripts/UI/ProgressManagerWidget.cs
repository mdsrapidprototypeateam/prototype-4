﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EProgressType { FINAL, CAP, CONTESTING, NONE };

public class ProgressManagerWidget : MonoBehaviour
{
    public EProgressType progressType = EProgressType.NONE;

    


    void Start ()
    {
        FindProgressUIComponents();

    }

    void Update ()
    {
        TrackTeamsProgress();   //get progress values from capPointManager
        UpdateUI();             //update UI with last values
    }


    private void FindProgressUIComponents()
    {
        

    }


    /// <summary>
    /// get progress values from capPointManager
    /// </summary>
    private void TrackTeamsProgress()
    {
        

    }

    /// <summary>
    /// Update UI widgets: progress bar and progress text
    /// </summary>
    void UpdateUI()
    {
        
    }

    /// <summary>
    /// Write integer as a formated string for progress bar UI
    /// </summary>
    /// <param name="value">int | the progress value</param>
    /// <returns>string | the formated string</returns>
    string FormatIntProgressString(int value)
    {
        string str = "";
        return str;
    }
}
