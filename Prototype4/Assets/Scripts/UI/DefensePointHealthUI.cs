﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefensePointHealthUI : MonoBehaviour
{
    private MyGameManager gameManager;
    public Image progressbar;
    public Palace palaceInfo;
    public float healthInit = 0;

    // Use this for initialization
    void Start ()
    {
        gameManager = MyGameManager.GetInstance();
        progressbar = GetComponent<Image>();
        if (palaceInfo)
        {
            healthInit = palaceInfo.health;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(!progressbar)
            progressbar = GetComponent<Image>();

        if (palaceInfo)
        {
            progressbar.fillAmount = palaceInfo.health / healthInit;
            if (progressbar.fillAmount <= 0.0f)
            {
                gameManager.e_FinishState = EFinishState.GAMEOVER;
                gameManager.ChangeToGameFinishState();
            }
        }
    }
}
