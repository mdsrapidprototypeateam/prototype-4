﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBase : MonoBehaviour {

	public bool attackRange;
	public float meleeProximity;
	public bool meleeInRange;
	public int attackDamage;
	public GameObject attackTarget;
	public int deviation;

	public List<GameObject> points; 
	public GameObject path;
	public float movSpeed;
	public float rotSpeed;
	public float proximityError;//the amount of space around the point before it is considered close enought to move to the next point


	public Vector3 deviationVec;

	// Use this for initialization
	void Start () 
	{
		FSM fsm = GetComponent<FSM> ();

		fsm.LoadState<UnitMoveState> ();
		fsm.LoadState<UnitAttackState> ();
		fsm.LoadState<UnitIddleState> ();

		fsm.ActivateState<UnitMoveState> ();

		deviationVec.x = Random.Range (-deviation, deviation);
		deviationVec.z = Random.Range (-deviation, deviation);
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}


	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("meleeRange"))
		{
			meleeInRange = true;
		}
	}

	void OnTriggerExit(Collider col)
	{
		if(col.CompareTag("meleeRange"))
		{
			meleeInRange = false;
		}
	}
    
}
