﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UnitStats : MonoBehaviour {

    public float health;
    public int goldReward;
    public bool hasGivenGold;
	public GameObject spawnerManager;
	public GameObject floatingTextPref;
	public GameObject cam;

	// Use this for initialization
	void Start () 
	{
		cam = GameObject.FindGameObjectWithTag ("MainCamera");
		spawnerManager = GameObject.FindGameObjectWithTag ("spawnerManager");
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0)
        {
            
            AudioManager.Instance.DeathSound(transform.position);
            AudioManager.Instance.GoldGained(transform.position);
			spawnerManager.GetComponent<UnitSpawner> ().unitsAlive -= 1;
			spawnerManager.GetComponent<UnitSpawner> ().unitsKilledThisWave += 1;
			Vector3 screenPos = cam.GetComponent<Camera> ().WorldToScreenPoint (transform.position);
			cam.GetComponent<PlaceObject> ().gold += goldReward;
		//	GameObject newText = Instantiate (floatingTextPref, transform.position, Quaternion.identity);
			Transform c = transform.Find("Hit_02");
			c.parent = null;
			c.transform.position = transform.position;
			c.gameObject.SetActive (true);
            Destroy(gameObject);
        }
	}
}
