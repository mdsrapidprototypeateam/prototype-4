﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSpawner : MonoBehaviour 
{
	public List<UnitWaveInfo> nextWaves;
	public bool active;
	public bool instantSpawn;
	public int currentWave;
	public int unitsAlive;
	public float waitTimer;
	private float innetWaitTimer;

	public List<GameObject> units;
	//public GameObject unitType1;
	//public GameObject unitType2;
	//public GameObject unitType3;


	public float innerTimer;

	public List<UnitInQueu> unitsInQueu;
	public float spawnUnitTimer;
	public int unitsSpawnThisWave;
	public int unitsKilledThisWave;
	private int extraWave;
	private bool newWaveStarted;
	public GameObject waveInfoText;
	public GameObject warningUp;
	public GameObject warningDown;
	public GameObject warningLeft;
	public GameObject warningRight;
	private GameObject lastSpawnWarning;
	private int multiplier;

	public float displayTextTime;
	private float innerdisplayTextTime;
	public bool loop;
	public GameObject gameManager;

	// Use this for initialization
	void Start () 
	{
		ObtainWaveInfos ();
	//	innerTimer = nextWaves [currentWave].spawnTimer;
        unitsInQueu = new List<UnitInQueu>();
		innetWaitTimer = waitTimer;
		multiplier = 1;
	
		//active = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
        //Set active only during GameStarted
        if(MyGameManager.GetInstance().e_State != EGameState.STATE_GAME_STARTED && active)
        {
            active = false;
        }
        else if (MyGameManager.GetInstance().e_State == EGameState.STATE_GAME_STARTED && !active)
        {
            active = true;
        }

        //Check for winning
        if (MyGameManager.GetInstance().e_State == EGameState.STATE_GAME_STARTED && active)
        {
			/*
            if (currentWave == transform.childCount - 1 && unitsAlive <= 0)
            {
                MyGameManager.GetInstance().e_FinishState = EFinishState.WINNER;
                MyGameManager.GetInstance().ChangeToGameFinishState();
            }
            */
            
        }


        if (instantSpawn) 
		{
			innerTimer = -1;
			instantSpawn = false;
		}
		if (active) 
		{
			//Spawner ();
			VerifyEndWave();
			NextWaveTrigger();
			EnemyQueu ();
			ActivateText ();
		}
	}


	void ObtainWaveInfos()
	{
		foreach (Transform child in transform)
		{
			nextWaves.Add (child.gameObject.GetComponent<UnitWaveInfo>());
		}

	}




	void NextWaveTrigger()
	{
		if (currentWave == 0 && !gameManager.GetComponent<GameManager> ().conversation1Trigger) 
		{
			gameManager.GetComponent<GameManager> ().conversation1Trigger = true;
		}
		if (innetWaitTimer > 0) 
		{
			innetWaitTimer -= Time.deltaTime;
		}
		else if(innetWaitTimer<=0 && innetWaitTimer>-10)
		{


			//next wave available
			if (unitsAlive == 0) 
			{
				
				if (currentWave == transform.childCount * multiplier) 
				{
					//end or loop
					if (loop) 
					{
						multiplier += 1;
					} 
					else 
					{
						//END GAME
						MyGameManager.GetInstance().e_FinishState = EFinishState.WINNER;
						MyGameManager.GetInstance().ChangeToGameFinishState();
					}
					
				} 
				else 
				{
					//next wave
					unitsKilledThisWave = 0;
					unitsSpawnThisWave = 0;
					currentWave += 1;
					extraWave = 0;
					innetWaitTimer = -20;
					AddNextWaveUnits ();
					newWaveStarted = false;
					lastSpawnWarning = null;
					waveInfoText.GetComponent<TMPro.TextMeshProUGUI> ().text = "Wave " + currentWave + " starting";
					innerdisplayTextTime = displayTextTime;
				}

			} 

		}

	}


	void VerifyEndWave()
	{
		// end of wave
		if (unitsKilledThisWave !=0 && unitsKilledThisWave == unitsSpawnThisWave && !newWaveStarted) 
		{
			innetWaitTimer = waitTimer;
			newWaveStarted = true;
			waveInfoText.GetComponent<TMPro.TextMeshProUGUI> ().text = "Wave " + currentWave  + " completed";
			innerdisplayTextTime = displayTextTime;
		}

	}

	void AddNextWaveUnits()
	{
		UnitWaveInfo newWave = nextWaves [(currentWave-(transform.childCount*(multiplier-1))) -1 + extraWave];
		int totalEnemies = newWave.numberOfUnits + (10 *(multiplier-1));
		Debug.Log (totalEnemies);
		int division = Random.Range (1, 5);
		Debug.Log ("division: " + division);

		List<int> unitCounts = new List<int>();
		List<int> paths= new List<int>();

		//separate the units per path
		for (int i = 0; i < division; i++) 
		{
			int j = Mathf.RoundToInt (totalEnemies / division);
			unitCounts.Add(j);
			Debug.Log ("added to unitcounts: " + totalEnemies / division);

			int newpath = Random.Range (0,newWave.spawner.GetComponent<UnitSpawnerInfo> ().pathsAvailable.Count);
			Debug.Log ("pathChoosen:" + newpath);
			paths.Add (newpath);
		}


		// add enemies to queu
		for (int i = 0; i < unitCounts.Count; i++) 
		{
			for (int j = 0; j < unitCounts [i]; j++) 
			{
				UnitInQueu newUnit = new UnitInQueu ();
				int type= CheckpPercents (newWave.percentPerUnits);
				Debug.Log (type);
				newUnit.type = type;
				newUnit.spawner = newWave.spawner;
				newUnit.path = paths[i];
				unitsInQueu.Add (newUnit);
				
				unitsSpawnThisWave += 1;
			}

		}

		if (newWave.addNextWave)
		{
			extraWave += 1;
			AddNextWaveUnits ();
		}




	}


	int CheckpPercents(List<int> percents)
	{
		int ranNum = Random.Range (1, 100);
		int currentPercent = 0;

		//unit zero add
		if (percents [0] != 0) 
		{
			currentPercent += percents [0];
			if (ranNum <= percents [0]) 
			{
				return 0;

			} 
		}

		//unit 1 add
		if (percents [1] != 0)
		{
			int oldcurrent = currentPercent;
			currentPercent += percents [1];
			if (ranNum > oldcurrent && ranNum <= currentPercent ) 
			{
				return 1;
			} 
		}

		//unit 2 add
		if (percents [2] != 0)
		{
			int oldcurrent = currentPercent;
			currentPercent += percents [2];
			if (ranNum > oldcurrent && ranNum <= currentPercent ) 
			{
				return 2;
			} 
		}

		//unit 3 add
		if (percents [3] != 0)
		{
			int oldcurrent = currentPercent;
			currentPercent += percents [3];
			if (ranNum > oldcurrent && ranNum <= currentPercent ) 
			{
				return 3;

			} 
		}
		//unit 4 add
		if (percents [4] != 0)
		{
			int oldcurrent = currentPercent;
			currentPercent += percents [4];
			if (ranNum > oldcurrent && ranNum <= currentPercent ) 
			{
				return 4;

			} 
		}
		//unit 5 add
		if (percents [5] != 0)
		{
			int oldcurrent = currentPercent;
			currentPercent += percents [5];
			if (ranNum > oldcurrent && ranNum <= currentPercent ) 
			{
				return 5;

			} 
		}

		return 0;
	}



	void SpawnEnemy(int type, GameObject spawner,int path)
	{
		if (lastSpawnWarning != spawner)
		{
			StartCoroutine(warningBlinkOFf(spawner.GetComponent<UnitSpawnerInfo>().id));
			lastSpawnWarning = spawner;

		}
			switch (type) 
			{
			case 0:
				{
					Debug.Log ("spawn unit type 1");
				GameObject newUnit = Instantiate (units[0], spawner.transform.position, Quaternion.identity,this.transform);
					newUnit.GetComponent<UnitBase>().path = spawner.GetComponent<UnitSpawnerInfo> ().pathsAvailable [path];
					break;
				}
			case 1:
				{
					Debug.Log ("spawn unit type 2");
				GameObject newUnit = Instantiate (units[1], spawner.transform.position, Quaternion.identity,this.transform);
					newUnit.GetComponent<UnitBase>().path = spawner.GetComponent<UnitSpawnerInfo> ().pathsAvailable [path];
				newUnit.SetActive (true);
					break;
				}

			case 2:
				{
					Debug.Log ("spawn unit type 3");
				GameObject newUnit = Instantiate (units[2], spawner.transform.position, Quaternion.identity,this.transform);
					newUnit.GetComponent<UnitBase>().path = spawner.GetComponent<UnitSpawnerInfo> ().pathsAvailable [path];
				newUnit.SetActive (true);
					break;
				}
		case 3:
			{
				Debug.Log ("spawn unit type 4");
				GameObject newUnit = Instantiate (units[3], spawner.transform.position, Quaternion.identity,this.transform);
				newUnit.GetComponent<UnitBase>().path = spawner.GetComponent<UnitSpawnerInfo> ().pathsAvailable [path];
				newUnit.SetActive (true);
				break;
			}
		case 4:
			{
				Debug.Log ("spawn unit type 5");
				GameObject newUnit = Instantiate (units[4], spawner.transform.position, Quaternion.identity,this.transform);
				newUnit.GetComponent<UnitBase>().path = spawner.GetComponent<UnitSpawnerInfo> ().pathsAvailable [path];
				newUnit.SetActive (true);
				break;
			}
		case 5:
			{
				Debug.Log ("spawn unit type 6");
				GameObject newUnit = Instantiate (units[5], spawner.transform.position, Quaternion.identity,this.transform);
				newUnit.GetComponent<UnitBase>().path = spawner.GetComponent<UnitSpawnerInfo> ().pathsAvailable [path];
				newUnit.SetActive (true);
				break;
			}

			}

		unitsAlive += 1;
			

	}



	void EnemyQueu()
	{
		if (unitsInQueu.Count != 0)
		{
			

			if (spawnUnitTimer < 0) 
			{
				Debug.Log ("I spawned something");
				//spawn
				SpawnEnemy (unitsInQueu [0].type, unitsInQueu [0].spawner, unitsInQueu [0].path);
				unitsInQueu.RemoveAt (0);
				//reset timer
				spawnUnitTimer = 0.5f;
			} 
			else 
			{
				spawnUnitTimer -= Time.deltaTime;
			}

		}
	}


	void ActivateText()
	{
		if (innerdisplayTextTime > 0) 
		{
			waveInfoText.SetActive (true);
			innerdisplayTextTime -= Time.deltaTime;
		} 
		else 
		{
			waveInfoText.GetComponent<TMPro.TextMeshProUGUI> ().text = "";
			waveInfoText.SetActive (false);
		}

	}

	IEnumerator warningBlinkOFf(int id)
	{
		switch (id)
		{
		case 0:
			warningUp.SetActive (true);
			break;
		case 1:
			warningDown.SetActive (true);
			break;
		case 2:
			warningLeft.SetActive (true);
			break;
		case 3:
			warningRight.SetActive (true);
			break;
		}

		yield return new WaitForSeconds (5f);

		switch (id)
		{
		case 0:
			warningUp.SetActive (false);
			break;
		case 1:
			warningDown.SetActive (false);
			break;
		case 2:
			warningLeft.SetActive (false);
			break;
		case 3:
			warningRight.SetActive (false);
			break;
		}
	}














}
