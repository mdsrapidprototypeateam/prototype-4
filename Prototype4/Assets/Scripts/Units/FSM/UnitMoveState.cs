﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMoveState : State
{
	private UnitBase unit;





	private Vector3 prePos;
	private int nextIntPos;

	public override void OnLoad()
	{
		unit = agent.GetComponent<UnitBase>();
		GetPath ();
		prePos=unit.points [0].transform.position + unit.deviationVec;
	}


	public override void OnEnterState ()
	{
		
	}

	public override void UpdateState ()
	{
		VerifyState ();
		BaseMovement ();
	}




	//obtains path from a given object
	// uses the path public gameobject set on the editor
	//obtains all its childs and inputs them into the points list
	void GetPath()
	{
		foreach (Transform child in unit.path.transform)
		{
			unit.points.Add (child.gameObject);
		}

	}



	//Movement function
	//follows a set of points set in the points array on the inspector
	//uses both the movSpeed and rotSpeed from the inspector
	//besides moving the object to the target also rotates to face the target
	void BaseMovement()
	{
		Vector3 nextPos = new Vector3 ();
		Vector3 newDir = new Vector3 ();
		//reset path

		if (unit.points.Count != 0) 
		{

			if (Vector3.Distance (agent.transform.position, prePos) >= unit.proximityError)
			{
				nextPos = prePos;
			} 
			else 
			{
				unit.points.RemoveAt (0);
				if (unit.points.Count != 0)
				{
					nextPos = unit.points [0].transform.position + unit.deviationVec;
					prePos = unit.points [0].transform.position + unit.deviationVec;
				}
			}

			agent.transform.position = Vector3.MoveTowards (agent.transform.position, nextPos, unit.movSpeed * Time.deltaTime);

			// make the object look at the target it is moving towards next
			newDir = new Vector3 (nextPos.x, agent.transform.position.y, nextPos.z) -agent.transform.position;
			//Debug.DrawRay (transform.position, newDir, Color.red);
			Quaternion finnishLook = Quaternion.LookRotation (newDir);
			finnishLook *= Quaternion.Euler (0, 90, 0);// becouse the object is currently rotated 90 degrees
			agent.transform.rotation = Quaternion.Slerp (agent.transform.rotation, finnishLook, unit.rotSpeed * Time.deltaTime);

		}

	}


	void VerifyState()
	{
		//exit condicion to attack state
		if (unit.attackRange == true) 
		{
			fsm.ActivateState<UnitAttackState> ();
			//agent.GetComponentInChildren<Animator> ().SetBool ("isFiring", true);
		}
		else if (unit.points.Count == 0) 
		{
			fsm.ActivateState<UnitIddleState> ();
		}


	}

	
}
