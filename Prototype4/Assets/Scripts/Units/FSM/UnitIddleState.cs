﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitIddleState : State
{
	private UnitBase unit;


	public override void OnLoad()
	{
		unit = agent.GetComponent<UnitBase>();

	}


	public override void OnEnterState ()
	{
		Debug.Log ("idle state active");
	}

	public override void UpdateState ()
	{
		VerifyState ();
	}




	void MoveTowardsTarget()
	{


	}

	void AttackTarget()
	{


	}



	void VerifyState()
	{
		//exit condicion to attack state
		if (unit.attackRange == true)
		{
			fsm.ActivateState<UnitAttackState> ();

		}
		else if (unit.points.Count != 0) 
		{
			fsm.ActivateState<UnitMoveState> ();
		}

	}

}
