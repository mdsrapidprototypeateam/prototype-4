﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class UnitAttackState : State
{
	private UnitBase unit;

	private float innerTimer;

	public override void OnLoad()
	{
		unit = agent.GetComponent<UnitBase>();

	}


	public override void OnEnterState ()
	{

	}

	public override void UpdateState ()
	{
		VerifyState ();

		if (unit.attackTarget) 
		{
			if (unit.meleeInRange) 
			{

				AttackTarget ();
			} 
			else 
			{
				if (Vector3.Distance (agent.transform.position, unit.attackTarget.transform.position) > unit.meleeProximity) 
				{
					MoveTowardsTarget ();
				} 
				else 
				{
					AttackTarget ();
				}
			}
		
		}

	}




	void MoveTowardsTarget()
	{
		Vector3 nextPos = unit.attackTarget.transform.position + unit.deviationVec;
		agent.transform.position = Vector3.MoveTowards (agent.transform.position, nextPos, unit.movSpeed * Time.deltaTime);

		// make the object look at the target it is moving towards next
		Vector3 newDir = new Vector3 (nextPos.x, agent.transform.position.y, nextPos.z) -agent.transform.position;
		//Debug.DrawRay (transform.position, newDir, Color.red);
		Quaternion finnishLook = Quaternion.LookRotation (newDir);
		finnishLook *= Quaternion.Euler (0, 90, 0);// becouse the object is currently rotated 90 degrees
		agent.transform.rotation = Quaternion.Slerp (agent.transform.rotation, finnishLook, unit.rotSpeed * Time.deltaTime);

	}

	void AttackTarget()
	{
		agent.transform.LookAt (unit.attackTarget.transform);
		if (innerTimer < 0) 
		{
			CameraShaker.Instance.ShakeOnce (5f, 6f, .05f, 0.3f);
			unit.attackTarget.GetComponent<DefenseUnit> ().health -= unit.attackDamage;

			Debug.Log ("I did zeh damage");
			innerTimer = 1;
		} 
		else 
		{
			innerTimer -= Time.deltaTime;
		}

	}



	void VerifyState()
	{
		//exit condicion to attack state
		if (unit.attackRange == false) 
		{
			fsm.ActivateState<UnitMoveState> ();
			agent.GetComponentInChildren<Animator> ().SetBool ("isFiring", false);
		}

	}


}
