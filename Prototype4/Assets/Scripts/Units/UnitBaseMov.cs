﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBaseMov : MonoBehaviour {


	public List<GameObject> points; 
	public GameObject path;
	public float movSpeed;
	public float rotSpeed;
	public float proximityError;//the amount of space around the point before it is considered close enought to move to the next point


	private Vector3 prePos;
	private int nextIntPos;
	// Use this for initialization
	void Start ()
	{
		GetPath ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		BaseMovement ();
		
	}



	//obtains path from a given object
	// uses the path public gameobject set on the editor
	//obtains all its childs and inputs them into the points list
	void GetPath()
	{
		foreach (Transform child in path.transform)
		{
			points.Add (child.gameObject);
		}

	}


	//Movement function
	//follows a set of points set in the points array on the inspector
	//uses both the movSpeed and rotSpeed from the inspector
	//besides moving the object to the target also rotates to face the target
	void BaseMovement()
	{
		Vector3 nextPos;
		Vector3 newDir;
		//reset path
	
		if (points.Count != 0) 
		{

			if (Vector3.Distance (transform.position, prePos) >= proximityError)
			{
				nextPos = prePos;
			} 
			else 
			{
				points.RemoveAt (0);
				nextPos = points [0].transform.position;
				prePos = points [0].transform.position;
			}

			transform.position = Vector3.MoveTowards (transform.position, nextPos, movSpeed * Time.deltaTime);

			// make the object look at the target it is moving towards next
			newDir = new Vector3 (nextPos.x, transform.position.y, nextPos.z) - transform.position;
			//Debug.DrawRay (transform.position, newDir, Color.red);
			Quaternion finnishLook = Quaternion.LookRotation (newDir);
			finnishLook *= Quaternion.Euler (0, 90, 0);// becouse the object is currently rotated 90 degrees
			transform.rotation = Quaternion.Slerp (transform.rotation, finnishLook, rotSpeed * Time.deltaTime);

		}

	}

	void OnCollisionEnter(Collision col)
	{
		if (col.collider.CompareTag ("TERRAIN")) 
		{
			//Debug.Log ("HELOOOOOOO");
		}
	
	}
}
