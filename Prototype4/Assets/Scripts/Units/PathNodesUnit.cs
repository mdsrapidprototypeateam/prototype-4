﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNodesUnit : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("tile"))
		{
			//Debug.Log ("i hit tiles");
			Vector3 nodePos = col.transform.position;
			nodePos.y = transform.position.y;
			this.transform.position = nodePos;
		}
	}
}
