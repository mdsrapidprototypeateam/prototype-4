﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum EGameState
{
    NONE,
    STATE_BOOTING,
    STATE_IDLE,
    STATE_MENU,
    STATE_INTRO,
    STATE_CUTSCENE,
    STATE_GAME_STARTED,
    STATE_FINISH
}

public enum EFinishState
{
    NONE,
    WINNER,
    GAMEOVER
}

public class MyGameManager : MonoBehaviour
{
    public EGameState e_State = EGameState.NONE;
    private EGameState m_EState;

    public delegate void EGameStateChanged(EGameState value);
    public event EGameStateChanged OnEGameStateChangedEvent;

    public EFinishState e_FinishState = EFinishState.NONE;

    public GameObject UI_CanvasTitle;
    public GameObject UI_CanvasTitleBackground;
    public GameObject UI_CanvasGameplay;
    public GameObject UI_CanvasIntro;
    public GameObject UI_CanvasFinish;
    //public GameObject[] PlayerObjects;
    //public GameObject[] PlayerTitleUIObjects;
    //public GameObject[] PlayerFinishUIObjects;
    //public GameObject PlayerUIObjects;
    //public Camera[] PlayerCameras;

    public float timeUntilReboot = 5.0f;

    public static MyGameManager GetInstance()
    {
		return GameObject.FindGameObjectWithTag("GAME_MANAGER").GetComponent<MyGameManager>();
    }

    void Start()
    {
        //Start tracking changes to game state
        StartEGameStateCounter();
        GetEGameState();

        //Set initial game state to idle
        if ( e_State.Equals(EGameState.NONE) )
            e_State = EGameState.STATE_BOOTING;

        GameObject uiParent = GameObject.FindWithTag("UI_PARENT");
        UI_CanvasTitleBackground = uiParent.transform.Find("Title_BG").gameObject;
        UI_CanvasTitle = uiParent.transform.Find("TitleScreen").gameObject;
        UI_CanvasIntro = uiParent.transform.Find("IntroScreen").gameObject;
        UI_CanvasGameplay = uiParent.transform.Find("GameplayHUD").gameObject;
        UI_CanvasFinish = uiParent.transform.Find("FinishScreen").gameObject;

        
    }


    void Update()
    {
        //Get current game state
        GetEGameState();
    }

    /// <summary>
    /// Auxiliary counter for tracking changes on EGameState property
    /// </summary>
    private void StartEGameStateCounter()
    {
        OnEGameStateChangedEvent += OnEGameStateChanged;
    }

    /// <summary>
    /// Get current EGameState public and private property values.
    /// Triggers event if they are not the same.
    /// /// </summary>
    public void GetEGameState()
    {
        if ( e_State != m_EState )
        {
            m_EState = e_State;

            if ( OnEGameStateChangedEvent != null )
                OnEGameStateChangedEvent(m_EState);
        }
    }

    /// <summary>
    /// What happens when EGameState change event is triggered
    /// </summary>
    /// <param name="value"></param>
    private void OnEGameStateChanged(EGameState value)
    {
        switch ( value )
        {
            case EGameState.STATE_BOOTING:
                {
                    UI_CanvasTitle.SetActive(false);
                    UI_CanvasGameplay.SetActive(false);
                    UI_CanvasIntro.SetActive(false);
                    UI_CanvasFinish.SetActive(false);
                    Invoke("ChangeToMenuState", timeUntilReboot);
                }
                break;

            case EGameState.STATE_MENU:
                {
                    OnGameMenu();
                }
                break;

            case EGameState.STATE_IDLE:
                {
                }
                break;

            case EGameState.STATE_INTRO:
                {
                    OnGameIntro();
                }
                break;

            case EGameState.STATE_CUTSCENE:
                {
                    Invoke("ChangeToGameStartedState", timeUntilReboot);
                }
                break;

            case EGameState.STATE_GAME_STARTED:
                {
                    OnGameStarted();
                }
                break;

            case EGameState.STATE_FINISH:
                {
                    OnGameFinished();
                }
                break;
        }
    }


    void ChangeToIdleState()
    {
        e_State = EGameState.STATE_IDLE;
    }

    void ChangeToMenuState()
    {
        e_State = EGameState.STATE_MENU;
    }

    void ChangeToIntroState()
    {
        e_State = EGameState.STATE_INTRO;
    }

    public void ChangeToCutsceneState()
    {
        e_State = EGameState.STATE_CUTSCENE;
    }

    public void ChangeToGameStartedState()
    {
        e_State = EGameState.STATE_GAME_STARTED;
    }

    public void ChangeToGameFinishState()
    {
        e_State = EGameState.STATE_FINISH;
    }

    void OnGameIntro()
    {
        Debug.Log("GAME MENU");
        UI_CanvasTitle.SetActive(false);
        UI_CanvasIntro.SetActive(true);
        UI_CanvasGameplay.SetActive(false);
        //Invoke("ChangeToGameStartedState", 2.0f);
    }

    void OnGameMenu()
    {
        Debug.Log("GAME MENU");
        UI_CanvasTitle.SetActive(true);
        UI_CanvasGameplay.SetActive(false);
        Invoke("ChangeToIntroState", 2.0f);
    }


    void OnGameStarted()
    {
        Debug.Log("GAME STARTED");
        UI_CanvasTitle.SetActive(false);
        UI_CanvasTitleBackground.SetActive(false);
        UI_CanvasIntro.SetActive(false);
        UI_CanvasGameplay.SetActive(true);
    }


    public void OnGameFinished()
    {
        //Stuff before rebooting
        Debug.Log("GAME FINISHED");
        UI_CanvasFinish.SetActive(true);
        Invoke("RebootGame", timeUntilReboot);
    }

    public void RebootGame()
    {
        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);
    }

    public void SkipIntro()
    {
        e_State = EGameState.STATE_GAME_STARTED;
        FMODUnity.RuntimeManager.GetBus("bus:/").stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }
}
