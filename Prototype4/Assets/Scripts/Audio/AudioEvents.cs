﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEvents : MonoBehaviour {

    [FMODUnity.EventRef]
    public string attack = "event:/EnemyAttack";

    [FMODUnity.EventRef]
    public string step = "event:/UnitStep";

    [FMODUnity.EventRef]
    public string horseStep = "event:/HorseStep";

    [FMODUnity.EventRef]
    public string goldGained = "event:/Gold";

    [FMODUnity.EventRef]
    public string towerShot = "event:/TowerShot";

    [FMODUnity.EventRef]
    public string construction = "event:/Construction";

    [FMODUnity.EventRef]
    public string deathSound = "event:/Death";

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AttackSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(attack, transform.position);
    }

    public void Footstep()
    {
        FMODUnity.RuntimeManager.PlayOneShot(step, transform.position);
    }

    public void HorseStep()
    {
        FMODUnity.RuntimeManager.PlayOneShot(horseStep, transform.position);
    }

    public void GoldGained()
    {
        FMODUnity.RuntimeManager.PlayOneShot(goldGained, transform.position);
    }

    public void TowerShot()
    {
        FMODUnity.RuntimeManager.PlayOneShot(towerShot, transform.position);
    }

    public void ConstructionSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(construction, transform.position);
    }

    public void DeathSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(deathSound, transform.position);
    }
}
