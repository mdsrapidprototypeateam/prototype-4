﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioManager Instance { get; private set; }




    bool shouldPlay = true;

    [FMODUnity.EventRef]
    public string battleMusic = "event:/BattleMusic";
    FMOD.Studio.EventInstance battleMusicEvent;

    [FMODUnity.EventRef]
    public string introMusic = "event:/IntroMusic";

    [FMODUnity.EventRef]
    public string attack = "event:/EnemyAttack";

    [FMODUnity.EventRef]
    public string goldGained = "event:/Gold";

    [FMODUnity.EventRef]
    public string towerShot = "event:/TowerShot";

    [FMODUnity.EventRef]
    public string rocketShot = "event:/RocketShot";

    [FMODUnity.EventRef]
    public string construction = "event:/Construction";

    [FMODUnity.EventRef]
    public string deathSound = "event:/Death";

    [FMODUnity.EventRef]
    public string spiderWalk = "event:/SpiderWalk";

    [FMODUnity.EventRef]
    public string canonMove = "event:/CanonMove";


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        StopAllEvents();
        battleMusicEvent = FMODUnity.RuntimeManager.CreateInstance(battleMusic);
        battleMusicEvent.start();
        battleMusicEvent.release();
    }
	
	// Update is called once per frame
	void Update () {
        
        


    }

    public void StopAllEvents()
    {
        FMODUnity.RuntimeManager.GetBus("bus:/").stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    public void AttackSound(Vector3 pos)
    {
        FMODUnity.RuntimeManager.PlayOneShot(attack, pos);
    }  

    public void GoldGained(Vector3 pos)
    {
        FMODUnity.RuntimeManager.PlayOneShot(goldGained, pos);
    }

    public void TowerShot(Vector3 pos)
    {
        FMODUnity.RuntimeManager.PlayOneShot(towerShot, pos);
    }

    public void RocketShot(Vector3 pos)
    {
        FMODUnity.RuntimeManager.PlayOneShot(rocketShot, pos);
    }

    public void ConstructionSound(Vector3 pos)
    {
        FMODUnity.RuntimeManager.PlayOneShot(construction, pos);
    }

    public void DeathSound(Vector3 pos)
    {
        FMODUnity.RuntimeManager.PlayOneShot(deathSound, pos);
    }

    public FMOD.Studio.EventInstance SpiderSound()
    {
        return FMODUnity.RuntimeManager.CreateInstance(spiderWalk);
    }

    public FMOD.Studio.EventInstance CanonMove()
    {
        return FMODUnity.RuntimeManager.CreateInstance(canonMove);
    }
}
