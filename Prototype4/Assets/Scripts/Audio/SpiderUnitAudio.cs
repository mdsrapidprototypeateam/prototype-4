﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderUnitAudio : MonoBehaviour {
    UnitBase unitBase;

    bool prevState = true;

    FMOD.Studio.EventInstance walk;
    FMOD.Studio.EventInstance attack;

    // Use this for initialization
    void Start()
    {
        unitBase = GetComponent<UnitBase>();
        walk = AudioManager.Instance.SpiderSound();
    }

    // Update is called once per frame
    void Update()
    {
        if (prevState != unitBase.meleeInRange)
        {
            if (unitBase.meleeInRange)
            {
                walk.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            }
            else
            {
                walk.start();
            }
        }

        if (!unitBase.meleeInRange)
        {
            walk.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        }

        prevState = unitBase.meleeInRange;
    }
   
}
