﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollingUnitAudo : MonoBehaviour {

    UnitBase unitBase;

    bool prevState = true;

    FMOD.Studio.EventInstance roll;
    FMOD.Studio.EventInstance attack;

    // Use this for initialization
    void Start () {
        unitBase = GetComponent<UnitBase>();
        roll = AudioManager.Instance.CanonMove();
	}
	
	// Update is called once per frame
	void Update () {
		if(prevState != unitBase.meleeInRange)
        {
            if (unitBase.meleeInRange)
            {
                roll.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            } else
            {
                roll.start();
            }
        }

        if (!unitBase.meleeInRange)
        {
            roll.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        }

        prevState = unitBase.meleeInRange;
    }
}
